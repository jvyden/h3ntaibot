require('dotenv').config();
const mineflayer = require('mineflayer')
const mcircbridge = require('./irc-minecraft-bridge')
const util = require('./util')

const single_user = (process.env.singleUser == 'true');
let dynamic = false;
if (!single_user) {
    dynamic = (process.env.dynamic == 'true');
}

console.log('Connecting to minecraft server...')
const hbot = {
    bot: mineflayer.createBot({
        host: process.env.mcHost,
        port: process.env.mcPort,
        username: process.env.mcUsername,
        password: process.env.mcPassword,
        version: '1.12.2'
    }),
}

hbot.bot.commands = {}
util.loadModules(hbot.bot, './commands', 'command')
util.loadModules(hbot,     './plugins', 'plugin')

hbot.bot.on('login', function () {
    console.log("Minecraft Bot connected.")
    mcircbridge(hbot.bot, {
        ircHost: process.env.ircHost,
        ircPort: process.env.ircPort,
        ircChannel: process.env.ircChannel,
        dynamic,
        single_user
    }, commandHandler)
})

hbot.bot.on('kicked', function(reason) {
    console.log(reason)
    process.exit(1)
})
hbot.bot.on('end', function() {
    process.exit(0)
})

const commandHandler = function (username, message) {
    if (username === hbot.bot.username) return;
    if (message.startsWith(process.env.commandPrefix)) {
        const args = message.substr(1).split(' ');
        console.log(username, 'ran', args[0])
        if (hbot.bot.commands[args[0]]) { // If the command specified exists in the array...
            // then run it with the arguments passed.
            hbot.bot.commands[args[0]](args)
        }
    }
}

hbot.bot.on('chat', commandHandler)