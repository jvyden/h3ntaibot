const nekoslife = require('nekos.life')
const client = new nekoslife();

const excludeArrFromArr = (arr, exclude) => {
    const newArr = [];
    arr.forEach((val) => {
        if (!exclude.includes(val)) {
            newArr.push(val)
        }
    })
    return newArr;
}

module.exports.command = (bot) => {
    bot.commands['neko'] = async function (args) {
        if (!args[1]) {
            const neko = await client.sfw.neko()
            bot.chat(`Your cat, kind sir. ${neko.url} (tip: ${process.env.commandPrefix}neko help for more)`)
        }
        else if (args[1].toLowerCase() === "help") {
            bot.chat('You can see the full list of NSFW and SFW modes at https://hastebin.com/uzaciguwix.txt')
        }
        else {
            if (client.sfw[args[1]]) {
                const neko = await client.sfw[args[1]]();
                bot.chat(`Your ${args[1]}, kind sir. ${neko.url}`);
            }
        }
    }
    bot.commands['nsfwneko'] = async function (args) {
        if (!args[1]) {
            const neko = await client.nsfw.neko()
            bot.chat(`Your lewds, kind sir. ${neko.url} (tip: ${process.env.commandPrefix}neko help for more)`)
        }
        else if (args[1].toLowerCase() === "help") {
            bot.chat('You can see the full list of NSFW and SFW modes at https://hastebin.com/uzaciguwix.txt')
        }
        else if (args[1].toLowerCase() === "furry") {
            bot.chat('degen')
        }
        else {
            if (client.nsfw[args[1]]) {
                const neko = await client.nsfw[args[1]]();
                bot.chat(`Your ${args[1]}, kind sir. ${neko.url}`);
            }
        }
    }
}
