module.exports.command = (bot) => {
    bot.commands['help'] = function() {
        const commands = Object.keys(bot.commands)
        bot.chat(`Commands (${commands.length}): ${commands.join(', ')}`)
    }
}