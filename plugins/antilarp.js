const larpcommands = [
    'firstwords',
    'firstjoin',
    'joindate',
    'lastwords',
    'lastseen',
    'seen',
    'jd',
    'pt'
]
module.exports.plugin = (hbot) => {
    hbot.bot.on('chat', (username, message) => {
        if (!(message.startsWith('!') || message.startsWith('?'))) return;
        if (larpcommands.includes(message.substr(1).split(' ')[0])) {
            hbot.bot.chat(">assuming larpbot is accurate")
        }
    })
}
