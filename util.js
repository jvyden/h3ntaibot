const fs = require('fs');
const path = require('path');

const getModules = (dir) => {
    let modules = {};
    let files = fs.readdirSync(dir);

    files.forEach(filename => {
        let filepath = path.resolve(path.join(dir, filename));
        let ext = path.extname(filename);
        let basename = path.basename(filename, ext);

        modules[basename] = require(filepath);
    });

    return modules;
};

module.exports.loadModules = (obj, dir, type) => {
    const modules = getModules(dir);
    Object.keys(modules).forEach(module => {
        if (modules[module][type] === undefined) return;
        console.log(`Loading ${type} ${module}`);
        modules[module][type](obj);
    });
    if (obj.modules != undefined) {
        const modules = Object.keys(obj.modules)
        console.log(`Loaded ${type}s: ${modules.join(', ')}`);
    }
};
